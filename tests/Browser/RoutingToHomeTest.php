<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\AboutPage;
use Tests\Browser\Pages\HomePage;
use Tests\DuskTestCase;

class RoutingToHomeTest extends DuskTestCase
{
	public function testRoutingToHomePage()
	{
		$this->browse(function (Browser $browser) {
			$browser->visit(new HomePage);
            $browser->screenshot('pages_home');
		});
	}
}
